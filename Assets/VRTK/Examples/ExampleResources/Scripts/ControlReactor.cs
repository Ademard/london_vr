﻿namespace VRTK.Examples
{
    using UnityEngine;
    using UnityEventHelper;

    public class ControlReactor : MonoBehaviour
    {
        public TextMesh go;
        public GameObject Control_panel;
        private VRTK_Control_UnityEvents controlEvents;
        public bool is_ventil1;

        private void Start()
        {
            controlEvents = GetComponent<VRTK_Control_UnityEvents>();
            if (controlEvents == null)
            {
                controlEvents = gameObject.AddComponent<VRTK_Control_UnityEvents>();
            }

            controlEvents.OnValueChanged.AddListener(HandleChange);
        }

        private void HandleChange(object sender, Control3DEventArgs e)
        {
            go.text = e.value.ToString() + "(" + e.normalizedValue.ToString() + "%)";
            if(is_ventil1)
                Control_panel.GetComponent<ControlPanel>().valve1_value = e.value;
            else
                Control_panel.GetComponent<ControlPanel>().valve2_value = e.value;
        }
    }
}