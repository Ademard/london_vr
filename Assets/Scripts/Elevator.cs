﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public bool player_inside;
    public GameObject lift_cabine;
    public GameObject player_prefab;
    public bool lift_up;
  
    private float timer;
    public float timer_lifting;
    public AudioClip lift_sound;
    private AudioSource aus;
    private bool audio_play;
    void Start()
    {
        aus = GetComponent<AudioSource>();
      
    }

    // Update is called once per frame
    void Update()
    {
       
        if (player_inside)
        {
            
            timer += Time.deltaTime;
            if (timer > 2 && !lift_up) { 
            lift_cabine.GetComponent<Animator>().enabled = true;
            lift_up = true;
            timer = 0;
            }
        }

        

        if (lift_up)
        {
            if (!audio_play)
            {
                aus.PlayOneShot(lift_sound);
                audio_play = true;
            }
            timer_lifting += Time.deltaTime;
            if (timer_lifting < 9.3)
            {
                player_prefab.transform.position += new Vector3(0, Time.deltaTime, 0);
            }
         
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player_prefab = other.gameObject;
            player_inside = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            player_inside = false;
    }
}
