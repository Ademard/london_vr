﻿using UnityEngine;
using System.Collections;

public class StairsUp : MonoBehaviour {		
	public GameObject player;
	private Vector3 move;
	public int speed;

	void OnTriggerStay(Collider other){
		if (other.gameObject.CompareTag (player.tag)) {
			//if(Input.GetAxis ("Vertical") != 0 || Input.GetAxis ("Horizontal") != 0) {
			move = new Vector3 (0,Input.GetAxis ("Vertical") + Input.GetAxis ("Horizontal"), 0);
			player.transform.position = player.transform.position + move/speed;
		} else {
			move = Vector3.zero;
		}
	}
}