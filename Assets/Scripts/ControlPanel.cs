﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanel : MonoBehaviour
{
    public GameObject arrow;
    public float valve1_value;
    public float valve2_value;
    public GameObject lamp_normal;
    public GameObject lamp_emergency;
    public ParticleSystem steam_fog;
    public GameObject water_splash;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (valve1_value > 0 || valve2_value > 0)
        {
            arrow.GetComponent<Animator>().SetTrigger("emergency");
            arrow.GetComponent<Animator>().ResetTrigger("fix_problem");
            lamp_emergency.SetActive(true);
            lamp_normal.SetActive(false);
            if (valve1_value > 0)
            {
                steam_fog.emissionRate = 200;
            }
            else
            {
                water_splash.SetActive(true);
            }
        }
        if (valve1_value == 0 && valve2_value == 0)
        {
            arrow.GetComponent<Animator>().SetTrigger("fix_problem");
            arrow.GetComponent<Animator>().ResetTrigger("emergency");
            lamp_emergency.SetActive(false);
            lamp_normal.SetActive(true);
            steam_fog.emissionRate = 10;
            water_splash.SetActive(false);
        }
    }
}
